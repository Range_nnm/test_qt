#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(const QString name): ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(name);
    dw = new DialogWindow();

    menuBar()->addMenu(scan = new QMenu(tr("Сканировать")));
    menuBar()->addMenu(options = new QMenu(tr("Настройки")));
    QAction* opt = options->addAction(tr("Настройки"));
    connect(opt, SIGNAL(triggered(bool)), this, SLOT(show_option_window()));

    menuBar()->addMenu(about = new QMenu(tr("О программе")));
    menuBar()->addMenu(info = new QMenu(tr("Использование")));

    //ui->menuBar->addAction(tr("Выход"), this, SLOT(close()));
    QAction* action = menuBar()->addAction(tr("Выход"));
    connect(action, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

    this->setGeometry( // окно по центру экрана
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

}

void MainWindow::showing()
{
    this->show();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scan;
    delete bar;
    delete dw;
}

// actions
void MainWindow::actionExit()
{
    this->close();
}

void MainWindow::show_option_window()
{
    this->dw->show();
}
