#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QAction>
#include <QDesktopWidget>

#include "dialogwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QString name);
    void showing();
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QMenuBar *bar;
    QMenu *scan;
    QMenu *options;
    QMenu *about;
    QMenu *info;
    DialogWindow* dw;

private slots:
    void actionExit();
    void show_option_window();
};

#endif // MAINWINDOW_H
