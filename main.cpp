// QT
#include <QApplication>
#include <QWidget>
#include <QMessageBox>
#include <QProcess>
#include <QtSql>

// 7z lib
#include <LzmaDec.h>

// c++
#include <string>
#include <fstream>

// my interface
#include <mainwindow.h>

using std::ifstream;
using std::ofstream;
using std::string;

// main function

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeMenuBar);

    MainWindow *w = new MainWindow("Распаковщик драйверов");
    w->show();

    // загрузка файла в память(более не требуется)
    /*ifstream file("test.7z", std::ios_base::binary);
    if(!file)
    {
        QMessageBox mess;
        mess.setText("Ошибка открытия файла");
        mess.exec();
        return 0;
    }
    file.seekg(0, std::ios_base::end); // прыгаем в конец файла
    int size(file.tellg()); // получаем смещение в байтах(размер файла)
    file.seekg(std::ios_base::beg); // прыгаем в начало файла
    char* buf = new char[size];
    for(int i(0);i<size;i++)
    {
        if(!file)
        {
            QMessageBox mess;
            mess.setText("Ошибка чтения файла");
            mess.exec();
            delete buf;
            return 0;
        }
        file.read(&buf[i], sizeof(char));
    }
    file.read(buf, size);
    string message(buf, buf + size);
    QMessageBox mess;
    mess.setText(message.c_str());
    mess.exec();

    ofstream f("test_1.7z");
    f.write(buf, size);
    delete buf;
    */


    // пример распаковки архива 7z, а именно его части(каталога)
    /*QProcess process;
    process.start("\"/home/range/Рабочий стол/OS/test_qt_gui/build-test-Desktop-Debug/7z\" x arc.7z Qt7z-master"); // распаковка части
    // добавить путь, куда будет распаковываться
    //process.start("\"/home/range/Рабочий стол/OS/test_qt_gui/build-test-Desktop-Debug/7z\" l -slt arch.7z"); // показать содержимое архива
    process.waitForFinished(-1); // сдвинуть команды на шаг назад
    QByteArray bytes = process.readAllStandardOutput(); // получить текст выполненной команды, чтобы записать его в лог
    //mess.setText(bytes);
    //mess.exec();*/


    // пример запроса получения нужных данных из бд с драйверами(запрос верен)
    /*QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName("/home/range/Рабочий стол/OS/DriverPack/index/db.sqlite");
    if(!sdb.open())
    {
        QMessageBox mess;
        mess.setText(sdb.lastError().text());
        mess.exec();
    }
    QSqlQuery a_query(sdb);
    QString line("select pack, directory, inf, date, system "
                 "from Drivers, Usable "
                 "where Usable.deviceId in (select id from Devices where deviceId = 'PCI\\VEN_8086&DEV_A33D') "
                 "and "
                 "Drivers.id in (select driverId from Sections where id in "
                 "(select sectionId from Usable where deviceId in "
                 "(select id from Devices where deviceId = 'PCI\\VEN_8086&DEV_A33D'))) "
                 "order by(date) desc");
    if (!a_query.exec(line)) {
        qDebug() << "Ошибочный запрос";
        return -2;
    }
    QSqlRecord rec = a_query.record();
    QString pack = "";
    QString directory = "";
    QString inf = "";
    QDateTime date;
    QString _system = "";

    while (a_query.next()) {
        pack = a_query.value(rec.indexOf("pack")).toString();
        directory = a_query.value(rec.indexOf("directory")).toString();
        inf = a_query.value(rec.indexOf("inf")).toString();
        date = a_query.value(rec.indexOf("date")).toDateTime();
        _system = a_query.value(rec.indexOf("[system]")).toString();

        qDebug() << pack << " | " << directory << " | " << inf << " | " << date.toString() << " | " << _system;
    }*/

    return app.exec();
}

// TODO: реализовать QMenu единичным пунктом, чтобы он НАЖИМАЛСЯ и исполнял действие
// реализовать остальные пункты меню(кроме сканировать)
// удалить 7z и попробовать распаковать 7zархив через программу, имея файл 7z в проекте
