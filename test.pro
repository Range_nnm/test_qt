#-------------------------------------------------
#
# Project created by QtCreator 2018-06-28T01:15:43
#
#-------------------------------------------------

QT       += core gui
QT       += sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    dialogwindow.cpp

HEADERS  += \
    mainwindow.h \
    dialogwindow.h

CONFIG += c++11

FORMS    += mainwindow.ui

unix:!macx: LIBS += "-L$$PWD/../../../../../home/range/Рабочий стол/OS/test_qt_gui/_7z_dec/build-lzma_decompiler-Desktop-Debug/" -llzma_decompiler

INCLUDEPATH += "$$PWD/../../../../../home/range/Рабочий стол/OS/test_qt_gui/_7z_dec/lzma_decompiler/src"
DEPENDPATH += "$$PWD/../../../../../home/range/Рабочий стол/OS/test_qt_gui/_7z_dec/build-lzma_decompiler-Desktop-Debug"

unix:!macx: PRE_TARGETDEPS += "$$PWD/../../../../../home/range/Рабочий стол/OS/test_qt_gui/_7z_dec/build-lzma_decompiler-Desktop-Debug/liblzma_decompiler.a"
