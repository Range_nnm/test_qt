#include "dialogwindow.h"

DialogWindow::DialogWindow(QWidget* parent): QDialog(parent)
{
    this->setWindowTitle("Настройки");
    this->hide();
    this->setModal(true);

    QHBoxLayout *first_line = new QHBoxLayout;
    first_line->addWidget(new QLabel("Путь к базе данных драйверов:"));
    first_line->addWidget(bd = new QLineEdit());

    QHBoxLayout *second_line = new QHBoxLayout;
    second_line->addWidget(new QLabel("Путь к каталогу вывода:"));
    second_line->addWidget(out = new QLineEdit());

    QHBoxLayout *third_line = new QHBoxLayout;
    app = new QPushButton("Применить");
    third_line->addWidget(app);
    cancel = new QPushButton("Отмена");
    third_line->addWidget(cancel);

    QVBoxLayout* all = new QVBoxLayout();
    all->addLayout(first_line);
    all->addLayout(second_line);
    all->addLayout(third_line);

    this->setFixedSize(400, 150);
    this->setLayout(all);
}

// Добавить две кнопки для открытия просмотра файлов(и каталогов)
// писать все в конфиг!!!
