#ifndef DIALOGWINDOW_H
#define DIALOGWINDOW_H

#include <QDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class DialogWindow : public QDialog
{
public:
    DialogWindow(QWidget* parent = nullptr);

private:
    QLineEdit* bd;
    QLineEdit* out;
    QPushButton* app;
    QPushButton* cancel;
};

#endif // DIALOGWINDOW_H
